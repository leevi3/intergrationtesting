// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", ()=>{
    describe("RBG to Hex convertions", ()=>{
        it("converts the basic colors", ()=>{
            const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redHex).to.equal("#ff0000"); // red hex value

            const greenHex = converter.rgbToHex(0, 255, 0); // #00ff00
            expect(greenHex).to.equal("#00ff00"); // blue hex value

            const blueHex = converter.rgbToHex(0, 0, 255); // #0000ff
            expect(blueHex).to.equal("#0000ff"); // blue hex value
        });
    });
});